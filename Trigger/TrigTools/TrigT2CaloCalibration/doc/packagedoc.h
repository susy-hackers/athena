/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/**

@page TrigT2CaloCalibration_page 
@author Carlos Osuna
@author Patricia Conde

@section TrigT2CaloCalibration_TrigT2CaloCalibrationOverview Overview
This package provides classes and methods to perform a Sampling Calibration 
for L2 trigger. With EM energy, HAD energy and eta as input, it provides
a fast calibration suitable for hadronic objects.



*/
